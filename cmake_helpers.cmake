# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <gilles.fernandez10@gmail.com> wrote this file. As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a (belgian) beer in
# return.
# Gilles Fernandez
# ----------------------------------------------------------------------------

include(ExternalProject)

macro(add_external_library NAME URL TAG)
    if (${URL} MATCHES ".*.git")
        file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME})

        ExternalProject_Add(
            ${NAME}
            GIT_REPOSITORY ${URL}
            GIT_TAG ${TAG}
            PATCH_COMMAND ""
            INSTALL_COMMAND ""
            SOURCE_DIR libs/${NAME}
            BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}
        )

        set(
            ${NAME}_INCLUDE_DIRS
            libs/${NAME}/include
        )

        set(
            ${NAME}_LIBRARIES
            ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}/${CMAKE_SHARED_LIBRARY_PREFIX}${NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}
        )

        set(
            ${NAME}_LIBRARY_PATH
            ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}
        )

        ExternalProject_Add_Step(
          ${NAME}
          CopyConfig
          COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}/config.h ${CMAKE_CURRENT_BINARY_DIR}/${NAME}-config.h
          DEPENDEES build
        )
    else()
        set(
            ${NAME}_INCLUDE_DIRS
            ${URL}/include
        )

        set(
            ${NAME}_LIBRARIES
            ${URL}/build/${CMAKE_SHARED_LIBRARY_PREFIX}${NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}
        )

        set(
            ${NAME}_LIBRARY_PATH
            ${URL}/build/
        )
    endif()

    list(APPEND EXTERNAL_LIBRARIES ${NAME})
endmacro()

macro(generate_version_variables MAJOR MINOR PATCH)
    string(TOUPPER ${PROJECT_NAME} UPPER_PROJECT_NAME)
    string(TIMESTAMP ${UPPER_PROJECT_NAME}_BUILD_TIMESTAMP "%Y-%m-%d %H:%M:%S")

    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.git)
        find_package(Git)

        if(GIT_FOUND)
            execute_process(
                COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
                WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
                OUTPUT_VARIABLE ${UPPER_PROJECT_NAME}_BUILD_BRANCH
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE
            )

            execute_process(
                COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
                WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
                OUTPUT_VARIABLE ${UPPER_PROJECT_NAME}_BUILD_COMMIT
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE
            )
      else()
        set(${UPPER_PROJECT_NAME}_BUILD_BRANCH "")
        set(${UPPER_PROJECT_NAME}_BUILD_COMMIT "")
      endif()
    endif()

    set(${UPPER_PROJECT_NAME}_NAME \"${PROJECT_NAME}\")
    set(${UPPER_PROJECT_NAME}_VERSION_MAJOR ${MAJOR})
    set(${UPPER_PROJECT_NAME}_VERSION_MINOR ${MINOR})
    set(${UPPER_PROJECT_NAME}_VERSION_PATCH ${PATCH})
    set(${UPPER_PROJECT_NAME}_VERSION ${${UPPER_PROJECT_NAME}_VERSION_MAJOR}.${${UPPER_PROJECT_NAME}_VERSION_MINOR}.${${UPPER_PROJECT_NAME}_VERSION_PATCH})
    set(${UPPER_PROJECT_NAME}_COPYRIGHT "Copyright (c) 2016 Gilles Fernandez" CACHE STRING "Copyright")
    set(${UPPER_PROJECT_NAME}_PROJECT_FOLDER ${CMAKE_CURRENT_SOURCE_DIR})
endmacro()
